type GlobalState = {
  autoEncode: boolean,
  saveContent: boolean
}

let globalState = {
  autoEncode: window.localStorage.getItem('auto-encode') === 'true',
  saveContent: window.localStorage.getItem('save-content') !== 'false'
}

export type { GlobalState };

export default globalState;